import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { LoadingController, ToastController, AlertController } from "@ionic/angular";
import { AppService } from "../app.service";
import { LoginService } from "./login.service";
import { Storage } from "@ionic/storage";
import { AuthenticationService } from '../services/authentication.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  isLogin = true;
  isLoading = false;
  fdata = "ngModel";
  f: any;

  credentials: any = {};
  constructor(
    private router: Router,
    private loadingCtrl: LoadingController,
    private authService: AppService,
    private authenticateService: AuthenticationService,
    private loginServ: LoginService,
    private ionicStorage: Storage,
    private toastController: ToastController
  ) { }
  // constructor(
  //   private loadingCtrl: LoadingController,
  //   private auth: AuthService,
  //   private router: Router,
  //   private alertCtrl: AlertController,
  //   private toastController: ToastController
  // ) { }

  ngOnInit() { }

  // login() {
  //   this.credentials = {
  //     email: 'saimon@devdactic.com',
  //     pw: '123'
  //   }
  //   this.auth.login(this.credentials).subscribe(async res => {
  //     if (res) {
  //       this.router.navigateByUrl('/maintabs');
  //     } else {
  //       const alert = await this.alertCtrl.create({
  //         header: 'Login Failed',
  //         message: 'Wrong credentials.',
  //         buttons: ['OK']
  //       });
  //       await alert.present();
  //     }
  //   });
  // }
  // onLogin() {
  //   this.isLoading = true;
  // }

  onSwitchAuthMode() {
    this.isLogin = !this.isLogin;
  }

  onSubmit(form: NgForm) {
    this.credentials = {};
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const pass = form.value.password;
    console.log(email, pass);
    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    var isEmail = validateEmail(email);
    var isName = isNaN(email);
    var strNum, data;

    if (isName === false) {
      strNum = email.trim();
    }

    if (isEmail === false && isName === false && strNum.length === 10) {
      this.credentials = {
        psd: pass,
        ph_num: email
      };
    } else if (isEmail) {
      this.credentials = {
        psd: pass,
        emailid: email
      };
    } else {
      this.credentials = {
        psd: pass,
        user_id: email
      };
    }

    // this.auth.login(this.credentials).subscribe(async res => {
    //   if (res) {
    //     this.router.navigateByUrl('/maintabs');
    //   } else {
    //     const alert = await this.alertCtrl.create({
    //       header: 'Login Failed',
    //       message: 'Wrong credentials.',
    //       buttons: ['OK']
    //     });
    //     await alert.present();
    //   }
    // });
    if (this.isLogin) {
      //send request to login servers
      this.loadingCtrl
        .create({
          keyboardClose: true,
          message: "Loging in..."
        })
        .then(loadingEl => {
          loadingEl.present();
          this.authService.authenticateUser(this.credentials).subscribe(
            response => {
              loadingEl.dismiss();
              form.reset();
              this.authenticateService.login()
              var obj = JSON.parse(
                window.atob(
                  JSON.parse(JSON.stringify(response)).token.split(".")[1]
                )
              );
              this.ionicStorage.set("token", obj).then(() => {
                localStorage.setItem("VeryFirstLoginUser", obj._id);
                // this.ionicStorage.set("VeryFirstLoginUser", obj._id).then(() => {
                  this.loginServ.login();
                  this.router.navigateByUrl("/maintabs/tabs/dashboard");
                // });
              });
            },
            err => {
              loadingEl.dismiss();
              this.toastController
                .create({
                  message: "Login Falied. Please use valid credentials...",
                  duration: 1500,
                  position: "bottom"
                })
                .then(toastEl => {
                  toastEl.present();
                });
            }
          );
        });
    } else {
      //send request to signup servers
    }
  }
}
