import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'https://www.oneqlik.in/gps', options: {} };
// const notifConfig: SocketIoConfig = { url: 'https://www.oneqlik.in/notifIO', options: {} }
import { URLs } from './app.model';
import { TabsService } from './tabs/tabs.service';
import { GoogleMaps } from '@ionic-native/google-maps/ngx';
import { AuthenticationService } from './services/authentication.service';
import { AuthGuard } from './services/auth.guard';
import { Push } from '@ionic-native/push/ngx';

// import { Injectable } from '@angular/core';
// import { SocketIoModule } from 'ngx-socket-io';
// import { Socket } from 'ngx-socket-io';

// @Injectable({providedIn: 'root'})
// export class SocketOne extends Socket {

//   constructor() {
//     super({ url: 'https://www.oneqlik.in/gps', options: {} });
//   }

// }

// @Injectable({providedIn: 'root'})
// export class SocketTwo extends Socket {

//   constructor() {
//     super({ url: 'https://www.oneqlik.in/notifIO', options: {} });
//   }

// }
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [
    // SocketOne, SocketTwo,
    StatusBar,
    SplashScreen,
    TabsService,
    URLs,
    GoogleMaps,
    AuthGuard,
    AuthenticationService,
    Push,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
