import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { tap, map } from "rxjs/operators";
import { Socket } from "ngx-socket-io";
import { URLs } from "./app.model";

@Injectable({
  providedIn: "root"
})
export class AppService {
  startLoading() {
    throw new Error("Method not implemented.");
  }
  loading: any;
  loading1: any;

  constructor(
    private http: HttpClient,
    private constUrl: URLs,
    private socket: Socket
  ) { }

  authenticateUser(authParams) {
    return this.http
      .post(this.constUrl.mainUrl + "users/LoginWithOtp", authParams)
      .pipe(
        tap(respData => {
          // console.log(respData)
        })
      );
  }

  getService(passedUrl) {
    return this.http.get(passedUrl).pipe(tap(respData => {
    }))
  }

  getServiceWithParams(passedUrl, data) {
    return this.http.post(passedUrl, data).pipe(tap(respData => {
    }))
  }

  getData(eventName) {
    return this.socket.fromEvent(eventName).pipe(map(data => data)); // with pipe rxjs6
  }

  getAddress(cord) {
    return this.http.post("https://www.oneqlik.in/googleAddress/getGoogleAddress", cord)
      .pipe(map(res => res));
  }
}
