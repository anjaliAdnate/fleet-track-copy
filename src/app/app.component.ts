import { Component, QueryList, ViewChildren } from '@angular/core';

import { Platform, IonRouterOutlet, ModalController, PopoverController, ActionSheetController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
// import { DashboardPage } from './tabs/dashboard/dashboard.page';
import { TabsService } from './tabs/tabs.service';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';
// import { AuthService } from './services/auth.service';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  // set up hardware back button event.
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  rootPage: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public tabs: TabsService,
    private authenticationService: AuthenticationService,
    private router: Router,
    // private alertController: AlertController,
    private toastController: ToastController,
    private modalCtrl: ModalController,
    private popoverCtrl: PopoverController,
    private actionSheetCtrl: ActionSheetController,
    // private auth: AuthService
    private push: Push
  ) {
    this.initializeApp();
    // Initialize BackButton Eevent.
    this.backButtonEvent();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.rootPage = DashboardPage;
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.authenticationService.authState.subscribe(state => {
        if (state) {
          this.router.navigate(['maintabs']);
        } else {
          this.router.navigate(['login']);
        }
      });

      // this.auth.authState.subscribe(state => {
      //   // debugger
      //   if (state) {
      //     this.router.navigate(['maintabs']);
      //   } else {
      //     this.router.navigate(['login']);
      //   }
      // });

      this.pushSetup();
    });
  }

  // active hardware back button
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      // close action sheet
      try {
        const element = await this.actionSheetCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close popover
      try {
        const element = await this.popoverCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
      }

      // close modal
      try {
        const element = await this.modalCtrl.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);

      }

      // close side menua
      // try {
      //   const element = await this.menu.getOpen();
      //   if (element !== null) {
      //     this.menu.close();
      //     return;

      //   }

      // } catch (error) {

      // }

      this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
        debugger
        if (outlet && outlet.canGoBack()) {
          // outlet.pop();
          this.router.navigateByUrl('/maintabs')

        } else if (this.router.url === '/login') {
          if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
            // this.platform.exitApp(); // Exit from app
            navigator['app'].exitApp(); // work for ionic 4

          } else {
            navigator['app'].exitApp();
            // if (this.router.isActive('/miantabs', true)) {
            // this.alertController.create({
            //   message: 'Do you want to exit the app?',
            //   buttons: [
            //     {
            //       text: 'YES PROCCED',
            //       handler: () => {
            //         navigator['app'].exitApp();
            //       }
            //     },
            //     {
            //       text: 'Back',
            //     }
            //   ]
            // }).then(alertEl => {
            //   alertEl.present();
            // })
            // }
            // this.toast.show(
            //   `Press back again to exit App.`,
            //   '2000',
            //   'center')
            //   .subscribe(toast => {
            //     // console.log(JSON.stringify(toast));
            //   });


            // this.toast.create({
            //   message: 'Press back again to exit App.',
            //   duration: 2000,
            //   position: 'middle'
            // }).then((toastEl => {
            //   toastEl.present();
            // }))
            this.lastTimeBackPress = new Date().getTime();
          }
        }
      });
    });
  }
  pushSetup() {
    // Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
    this.push.createChannel({
      id: "ignitionon",
      description: "ignition on description",
      sound: 'ignitionon',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('ignitionon Channel created'));

    this.push.createChannel({
      id: "ignitionoff",
      description: "ignition off description",
      sound: 'ignitionoff',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('ignitionoff Channel created'));

    this.push.createChannel({
      id: "devicepowerdisconnected",
      description: "devicepowerdisconnected description",
      sound: 'devicepowerdisconnected',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('devicepowerdisconnected Channel created'));

    this.push.createChannel({
      id: "default",
      description: "default description",
      sound: 'default',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('default Channel created'));

    this.push.createChannel({
      id: "sos",
      description: "default description",
      sound: 'notif',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('sos Channel created'));

    // Delete a channel (Android O and above)
    // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));

    // Return a list of currently configured channels
    this.push.listChannels().then((channels) => console.log('List of channels', channels))

    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {},
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    }

    const pushObject: PushObject = that.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => {
      console.log('Received a notification', notification)
      if (notification.additionalData.foreground) {
        this.toastController.create({
          message: notification.message,
          duration: 2000
        }).then((toastEl) => {
          toastEl.present();
        });
      }
    });

    pushObject.on('registration').subscribe((registration: any) => {
      console.log('Device registered', registration)
      localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
    });

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }


  // pushSetup() {
  //   // to check if we have permission
  //   this.push.hasPermission()
  //     .then((res: any) => {

  //       if (res.isEnabled) {
  //         console.log('We have permission to send push notifications');
  //       } else {
  //         console.log('We do not have permission to send push notifications');
  //       }

  //     });

  //   // to initialize push notifications

  //   const options: PushOptions = {
  //     android: {},
  //     ios: {
  //       alert: 'true',
  //       badge: true,
  //       sound: 'false'
  //     },
  //     windows: {},
  //     browser: {
  //       pushServiceURL: 'http://push.api.phonegap.com/v1/push'
  //     }
  //   }

  //   const pushObject: PushObject = this.push.init(options);


  //   pushObject.on('notification').subscribe((notification: any) => {
  //     console.log('Received a notification', notification)
  //     if (notification.additionalData.foreground) {
  //       this.toastController.create({
  //         message: notification.message,
  //         duration: 2000
  //       }).then((toastEl) => {
  //         toastEl.present();
  //       });
  //     }
  //   });

  //   pushObject.on('registration').subscribe((registration: any) => {
  //     console.log('Device registered', registration)
  //     localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
  //   });

  //   pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));


  // }
}
