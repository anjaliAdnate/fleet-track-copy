import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NavController, LoadingController, ModalController, Events } from '@ionic/angular';
// import { Socket } from 'ngx-socket-io';
import { Storage } from '@ionic/storage';
import { AppService } from 'src/app/app.service';
import { URLs } from 'src/app/app.model';
import { LiveComponent } from './live/live.component';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
// import { SocketTwo } from 'src/app/app.module';
import { Socket, SocketIoConfig } from 'ngx-socket-io';
// declare var $;
// import { Environment, GoogleMaps, GoogleMap } from '@ionic-native/google-maps';
declare var google;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss']
})
export class DashboardPage implements OnInit {
  @ViewChild('mapElement', { static: true }) mapElement: ElementRef;
  public cartCount: number = 0;
  map;
  userToken: any;
  to: string;
  from: string;
  dashdata: any = {};
  vehData: any[];
  user = null;
  // socket: any;

  constructor(
    // private platform: Platform,
    public navCtrl: NavController,
    private ionicStorage: Storage,
    private appService: AppService,
    private constUrl: URLs,
    private loadingCtrl: LoadingController,
    private modalController: ModalController,
    private router: Router,
    private ngZone: NgZone,
    private auth: AuthService,
    private events: Events,
    private io: Socket
  ) {
    
  }

  ionViewWillEnter() {
    // this.user = this.auth.getUser();
    // console.log("decoded jwt token: " + JSON.stringify(this.user));
    this.to = new Date().toISOString();
    var d = new Date();
    var a = d.setHours(0, 0, 0, 0)
    this.from = new Date(a).toISOString();
    this.ionicStorage.get('token').then((val) => {
      console.log('Your age is', val);
      this.userToken = val;
      this.getDashData();
      this.notifs();
    })
  }

  seeNotifications() {

  }

  ngOnInit(): void {
    // this.to = new Date().toISOString();
    // var d = new Date();
    // var a = d.setHours(0, 0, 0, 0)
    // this.from = new Date(a).toISOString();
    // this.ionicStorage.get('token').then((val) => {
    //   console.log('Your age is', val);
    //   this.userToken = val;
    //   this.getDashData();
    // })
  }
  ngAfterContentInit(): void {
    this.map = new google.maps.Map(
      this.mapElement.nativeElement,
      {
        disableDefaultUI: true
      }
    );
  }

  connect(url: string) {
    // if (this.io) this.disconnect();
    // debugger
    const socketIoConfig: SocketIoConfig = { url };
    this.io = new Socket(socketIoConfig);
    this.io.connect();

    this.io.on(this.userToken._id, (msg) => {
      this.cartCount += 1;
      console.log("got notif ping: ", msg);
    } );
    this.io.on("error", (msg) => {
      console.log("socket connection error: ", msg)
    });
  }

  notifs() {
    ///////////////=======Push notifications========////////////////
    this.events.subscribe('cart:updated', (count) => {
      this.cartCount = count;
    });
    this.connect("https://www.oneqlik.in/notifIO");
  }

  gotoDetail(key) {
    console.log('check key: ', key)
    this.router.navigateByUrl('/maintabs/tabs/vehicle-list/' + key);
  }

  getDashData() {
    var _baseUrl = this.constUrl.mainUrl + 'gps/getDashboard?email=' + this.userToken.email + '&from=' + this.from + '&to=' + this.to + '&id=' + this.userToken._id;
    if (this.userToken.isSuperAdmin == true) {
      _baseUrl += '&supAdmin=' + this.userToken._id;
    } else {
      if (this.userToken.isDealer == true) {
        _baseUrl += '&dealer=' + this.userToken._id;
      }
    }
    this.loadingCtrl.create({
      keyboardClose: true, message: 'Loging in...'
    }).then(loadingEl => {
      loadingEl.present();
      this.appService.getService(_baseUrl)
        .subscribe(respData => {
          loadingEl.dismiss();
          console.log("dashboard resp data: ", respData);
          this.dashdata = respData;

        })
    });
    this.getVehicleList();
  }

  getVehicleList() {
    let that = this;
    var _baseUrl = this.constUrl.mainUrl + 'devices/getDeviceByUser?id=' + this.userToken._id + '&email=' + this.userToken.email;
    if (this.userToken.isSuperAdmin == true) {
      _baseUrl += '&supAdmin=' + this.userToken._id;
    } else {
      if (this.userToken.isDealer == true) {
        _baseUrl += '&dealer=' + this.userToken._id;
      }
    }

    this.loadingCtrl.create({
      keyboardClose: true, message: 'Loging in...'
    }).then(loadingEl => {
      loadingEl.present();
      this.appService.getService(_baseUrl)
        .subscribe(respData => {
          loadingEl.dismiss();
          this.map.addListener('click', function (e) {
            console.log("clicked on map!!");
            that.ngZone.run(() => {
              that.router.navigateByUrl('/maintabs/tabs/dashboard/live');
            });

            // that.presentModal();
            // this.map.fullscreenControl();
            // debugger
            // var map = document.querySelector(".gm-style");
            // map.requestFullscreen();
            // (<HTMLDivElement>this.mapElement.nativeElement).style.marginTop = offset + 'px';
          });
          this.vehData = respData['devices'].map(function (d) {
            if (d.last_location !== undefined) {
              return d;
            } else {
              return null;
            }
          });
          var filtered = this.vehData.filter(function (el) {
            return el != null;
          });
          console.log("sorted resp data: ", filtered);


          var shape = {
            coord: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
          };
          var infowindow = new google.maps.InfoWindow();
          var bounds = new google.maps.LatLngBounds();
          for (var i = 0; i < filtered.length; i++) {
            var image;
            if (filtered[i].status.toLowerCase() !== 'expired') {
              if (filtered[i].iconType !== null) {
                if (filtered[i].status.toLowerCase() === 'running' && filtered[i].last_speed > 0) {
                  image = new google.maps.MarkerImage('./assets/imgs/vehicles/running' + filtered[i].iconType + '.png',
                    new google.maps.Size(20, 40),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(0, 40));
                } else {
                  if (filtered[i].status.toLowerCase() === 'running' && filtered[i].last_speed === 0) {
                    image = new google.maps.MarkerImage('./assets/imgs/vehicles/idling' + filtered[i].iconType + '.png',
                      new google.maps.Size(20, 40),
                      new google.maps.Point(0, 0),
                      new google.maps.Point(0, 40));
                  } else {
                    image = new google.maps.MarkerImage('./assets/imgs/vehicles/' + filtered[i].status.toLowerCase() + filtered[i].iconType + '.png',
                      new google.maps.Size(20, 40),
                      new google.maps.Point(0, 0),
                      new google.maps.Point(0, 40));
                  }
                }
                var beach = filtered[i];
                var myLatLng = new google.maps.LatLng(beach.last_location.lat, beach.last_location.long);
                var marker = new google.maps.Marker({
                  position: myLatLng,
                  map: this.map,
                  icon: image,
                  shape: shape,
                  title: beach.Device_Name,
                  zIndex: i + 1
                });
                bounds.extend(myLatLng);

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                  return function () {
                    infowindow.setContent(filtered[i].Device_Name);
                    infowindow.open(this.map, marker);
                  }
                })(marker, i));
              }
            }
          }
          this.map.fitBounds(bounds);
        })
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: LiveComponent
    });
    return await modal.present();
  }

  // ionViewDidLoad() {
  //   this.loadMap();
  // }
  // loadMap() {
  //   console.log('Inside the function');
  //   const latLng = new google.maps.LatLng(-34.929, 138.601);
  //   const mapOptions = {
  //     center: latLng,
  //     zoom: 15,
  //     mapTypeId: google.maps.MapTypeId.ROADMAP
  //   };
  //   this.map = new google.maps.Map(this.mapElement, mapOptions);
  //   //   Environment.setEnv({
  //   //     'API_KEY_FOR_BROWSER_RELEASE' : 'AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8',
  //   //     'API_KEY_FOR_BROWSER_DEBUG' : 'AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8',
  //   //   });
  //   //   this.map = GoogleMaps.create('map_canvas');
  // }
}
