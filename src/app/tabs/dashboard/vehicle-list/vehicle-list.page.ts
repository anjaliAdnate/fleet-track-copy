import { Component, OnInit } from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
import { AppService } from "src/app/app.service";
import { LoadingController, ToastController } from "@ionic/angular";
import { URLs } from "src/app/app.model";
import { DataService } from "src/app/services/data.service";
import { GeocoderAPIService } from 'src/app/geocoder-api.service';

@Component({
  selector: "app-vehicle-list",
  templateUrl: "./vehicle-list.page.html",
  styleUrls: ["./vehicle-list.page.scss"]
})
export class VehicleListPage implements OnInit {
  segment: string = "all";
  userToken: any;
  stat: any;
  pageNo: number = 0;
  limit: number = 6;
  vehicleList: any = [];
  infiniteScroll: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private ionicStorage: Storage,
    private appService: AppService,
    private loadingController: LoadingController,
    private constUrl: URLs,
    private toastController: ToastController,
    private dataService: DataService, // public events: Events
    private geocoderApi: GeocoderAPIService
  ) { }

  ionViewWillEnter() {
    this.ionicStorage.get("token").then(val => {
      this.userToken = val;
      console.log("user token: ", this.userToken);

      if (this.activatedRoute.snapshot.paramMap.get('id')) {
        console.log("activated route param map: ", this.activatedRoute.snapshot.paramMap.get('id'));
        const idKey = this.activatedRoute.snapshot.paramMap.get('id');
        if (idKey === 'moving') {
          this.segment = idKey;
        } else if (idKey === 'stopped') {
          this.segment = idKey;
        } else if (idKey === 'idling') {
          this.segment = idKey;
        } else {
          this.segment = idKey;
        }
        this.vehicleList = [];
        this.getVehicleList();
      } else {
        this.segment = 'all';
        this.vehicleList = [];
        this.getVehicleList();
      }

    });
  }

  ngOnInit() { }

  doInfinite(infiniteScroll) {
    this.infiniteScroll = infiniteScroll;
    this.pageNo = this.pageNo + 1;
    setTimeout(() => {
      this.getVehicleList();
    }, 500);
  }

  onSegmentChange(ev: any) {
    this.stat = undefined;
    this.pageNo = 0;
    this.limit = 6;
    this.vehicleList = [];
    this.infiniteScroll = undefined;
    this.getVehicleList();
  }

  getVehicleList() {
    console.log("infinite scroll element: ", this.infiniteScroll)
    var baseURLp;
    if (this.segment === "moving") {
      this.stat = "RUNNING";
    } else if (this.segment === "stopped") {
      this.stat = "STOPPED";
    } else if (this.segment === "idling") {
      this.stat = "IDLING";
    }
    if (this.stat) {
      baseURLp =
        this.constUrl.mainUrl +
        "devices/getDeviceByUser?id=" +
        this.userToken._id +
        "&email=" +
        this.userToken.email +
        "&statuss=" +
        this.stat +
        "&skip=" +
        this.pageNo +
        "&limit=" +
        this.limit;
    } else {
      baseURLp =
        this.constUrl.mainUrl +
        "devices/getDeviceByUser?id=" +
        this.userToken._id +
        "&email=" +
        this.userToken.email +
        "&skip=" +
        this.pageNo +
        "&limit=" +
        this.limit;
    }

    if (this.userToken.isSuperAdmin == true) {
      baseURLp += "&supAdmin=" + this.userToken._id;
    } else {
      if (this.userToken.isDealer == true) {
        baseURLp += "&dealer=" + this.userToken._id;
      }
    }

    if (!this.infiniteScroll) {
      this.loadingController
        .create({
          message: "Loading Vehicle List..."
        })
        .then(loadingEl => {
          loadingEl.present();
          this.appService.getService(baseURLp).subscribe(
            resp => {
              loadingEl.dismiss();
              console.log("vehicle list: ", resp);
              this.vehicleList = JSON.parse(JSON.stringify(resp)).devices;
            },
            err => {
              loadingEl.dismiss();
              this.toastController
                .create({
                  message: "Something went wrong. Please try after some time..",
                  position: "bottom",
                  duration: 1500
                })
                .then(toastEl => {
                  toastEl.present();
                });
            }
          );
        });
    } else {
      this.appService.getService(baseURLp).subscribe(
        resp => {
          let that = this;
          var data = JSON.parse(JSON.stringify(resp)).devices;
          for (let i = 0; i < data.length; i++) {
            that.vehicleList.push(data[i]);
            //console.log("vehicle list length: ", that.vehicleList.length);
          }
          this.infiniteScroll.target.complete();
        },
        err => {
          this.toastController
            .create({
              message: "Something went wrong. Please try after some time..",
              position: "bottom",
              duration: 1500
            })
            .then(toastEl => {
              toastEl.present();
            });
          this.infiniteScroll.target.complete();
        }
      );
    }

  }

  onVehDetail(veh) {
    this.ionicStorage.set("vehicleToken", veh);
    this.dataService.setData(42, veh);
    this.router.navigateByUrl(
      "/maintabs/tabs/vehicle-list/second-tabs/second-main-tabs/live-track/42"
    );
  }

  device_address(device, index) {
    let that = this;
    that.vehicleList[index].address = "N/A";
    if (!device.last_location) {
      that.vehicleList[index].address = "N/A";
      return;
    }
    let tempcord = {
      "lat": device.last_location.lat,
      "long": device.last_location.long
    }
    this.appService.getAddress(tempcord)
      .subscribe(resp => {
        var res = JSON.parse(JSON.stringify(resp));
        if (res.message === "Address not found in databse") {
          this.geocoderApi.reverseGeocode(device.last_location.lat, device.last_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.vehicleList[index].address = str;
            })
        } else {
          that.vehicleList[index].address = res.address;
        }
      });
  }
}
