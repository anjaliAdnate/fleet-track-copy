// import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import { Socket } from 'ngx-socket-io';
// @Component({
//   selector: 'app-live',
//   templateUrl: './live.component.html',
//   styleUrls: ['./live.component.scss'],
// })
// export class LiveComponent implements OnInit {
//   @ViewChild('mapElement', { static: true }) mapElement: ElementRef;
//   map;
//   constructor(
//     private socket: Socket
//   ) { }

//   ngOnInit() {
//     this.socket.connect();
//   }
//   ngAfterContentInit(): void {
//   }

// }
import { AppService } from 'src/app/app.service';
import { Component, OnInit, OnDestroy, ElementRef, NgZone, ViewChild } from '@angular/core';
import { NavController, ActionSheetController, AlertController, ModalController, Platform, ToastController, LoadingController } from '@ionic/angular';
// import { AppService } from '../../providers/api-service/api-service';
// import { SocialSharing } from '@ionic-native/social-sharing';
// import * as io from 'socket.io-client';
import * as _ from "lodash";

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  GoogleMapsMapTypeId,
  LatLngBounds,
  LatLng,
  Spherical
} from '@ionic-native/google-maps/ngx';
// import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, GoogleMapsMapTypeId, LatLngBounds, ILatLng, Polygon, GoogleMapsAnimation } from '@ionic-native/google-maps';
// import { TranslateService } from '@ngx-translate/core';
import { DrawerState } from 'ion-bottom-drawer';
import { URLs } from 'src/app/app.model';
import { Storage } from '@ionic/storage';
import { Socket } from 'ngx-socket-io';
// import { SocketOne } from 'src/app/app.module';
// import { Subscription } from 'rxjs/Subscription';
// import { PoiPage } from '../live-single-device/live-single-device';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss'],
})
export class LiveComponent implements OnInit, OnDestroy {
  @ViewChild('map', {
    static: true
  }) element: ElementRef;
  map: GoogleMap;

  loading: any;

  shouldBounce = true;
  dockedHeight = 52;
  distanceTop = 150;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 52;

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};

  data: any = {};

  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: any;
  portstemp: any;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  resToken: string;
  liveDataShare: any;
  isEnabled: boolean = false;
  showMenuBtn: boolean = false;
  latlngCenter: any;
  mapHideTraffic: boolean = false;
  mapData: any = [];
  last_ping_on: any;
  geodata: any = [];
  geoShape: any = [];
  generalPolygon: any;
  locations: any = [];
  acModel: any;
  locationEndAddress: any;
  tempaddress: any;
  mapKey: string;
  shwBckBtn: boolean = false;
  recenterMeLat: any;
  recenterMeLng: any;
  menuActive: boolean;
  deviceDeatils: any = {};
  impkey: any;
  showaddpoibtn: boolean = false;

  constructor(
    public googleMaps: GoogleMaps,
    public navCtrl: NavController,
    // public navParams: NavParams,
    public appService: AppService,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    private ionicStorage: Storage,
    // private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public plt: Platform,
    // private viewCtrl: ViewController,
    private constURL: URLs,
    // private translate: TranslateService,
    private toastCtrl: ToastController,
    private socket: Socket,
    private loadingCtrl: LoadingController,
    private ngZone: NgZone
  ) {
    // var selectedMapKey;
    // if (localStorage.getItem('MAP_KEY') != null) {
    //   selectedMapKey = localStorage.getItem('MAP_KEY');
    //   if (selectedMapKey == this.translate.instant('hybrid')) {
    //     this.mapKey = 'MAP_TYPE_HYBRID';
    //   } else if (selectedMapKey == this.translate.instant('normal')) {
    //     this.mapKey = 'MAP_TYPE_NORMAL';
    //   } else if (selectedMapKey == this.translate.instant('terrain')) {
    //     this.mapKey = 'MAP_TYPE_TERRAIN';
    //   } else if (selectedMapKey == this.translate.instant('satellite')) {
    //     this.mapKey = 'MAP_TYPE_SATELLITE';
    //   }
    // } else {
    //   this.mapKey = 'MAP_TYPE_NORMAL';
    // }

    // this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    // console.log("user details=> " + this.userdetails);
    // this.menuActive = false;
  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';

  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
  };

  icons = {
    "car": this.carIcon,
    "bike": this.carIcon,
    "truck": this.carIcon,
    "bus": this.carIcon,
    "user": this.carIcon,
    "jcb": this.carIcon,
    "tractor": this.carIcon
  }

  ionViewDidEnter() {
    this.socket.connect();
    // this._io = io.connect(this.constURL.mainUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    // this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    console.log("call ionViewDidLoad");
    this.plt.ready();
    this.ngZone.run(() => this.initMap());
    this.ionicStorage.get('token').then((val) => {
      console.log('Your age is', val);
      this.userdetails = val;
      this.showBtn = false;
      this.SelectVehicle = "Select Vehicle";
      this.selectedVehicle = undefined;
      this.userDevices();
    });
  }

  initMap() {
    this.map = GoogleMaps.create(this.element.nativeElement);
    this.map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {
      this.onButtonClick();
      // let coordinates: LatLng = new LatLng(36.06731743465648, -79.79521393775941);
      // let position = {
      //   target: coordinates,
      //   zoom: 17
      // };
      // this.map.animateCamera(position);
      // let markerOptions: MarkerOptions = {
      //   position: coordinates,
      //   title: 'Greensboro, NC'
      // };
      // const marker = this.map.addMarker(markerOptions)
      //   .then((marker: Marker) => {
      //     marker.showInfoWindow();
      //   });
    })
  }

  ngOnInit() { }

  newMap() {
    let mapOptions = {
      controls: {
        zoom: false
      },
      mapTypeControlOptions: {
        mapTypeIds: [GoogleMapsMapTypeId.ROADMAP, 'map_style']
      },
      gestures: {
        rotate: false,
        tilt: false
      },
      mapType: this.mapKey
    }
    let map = GoogleMaps.create('map_canvas', mapOptions);
    if (this.plt.is('android')) {
      map.animateCamera({
        target: { lat: 20.5937, lng: 78.9629 },
        duration: 2000,
        // padding: 10,  // default = 20px
      })
      map.setPadding(20, 20, 20, 20);
    }
    return map;
  }

  async onButtonClick() {
    this.map.clear();

    this.loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    await this.loading.present();

    // Get the location of you
    this.map.getMyLocation().then((location: MyLocation) => {
      this.loading.dismiss();
      console.log(JSON.stringify(location, null, 2));

      // Move the map camera to the location with animation
      // this.map.animateCamera({
      //   target: location.latLng,
      //   zoom: 17,
      //   tilt: 30
      // });

      // add a marker
      let marker: Marker = this.map.addMarkerSync({
        title: '@ionic-native/google-maps plugin!',
        snippet: 'This plugin is awesome!',
        position: location.latLng,
        animation: GoogleMapsAnimation.BOUNCE
      });

      // show the infoWindow
      marker.showInfoWindow();

      // If clicked it, display the alert
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        this.showToast('clicked!');
      });
    })
      .catch(err => {
        this.loading.dismiss();
        this.showToast(err.error_message);
      });
  }

  userDevices() {
    var baseURLp;
    let that = this;
    // if (that.allData.map != undefined) {
    //   that.allData.map.remove();
    //   that.allData.map = that.newMap();
    // } else {
    //   that.allData.map = that.newMap();
    // }

    baseURLp = this.constURL.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;

    if (this.userdetails.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.userdetails._id;
    } else {
      if (this.userdetails.isDealer == true) {
        baseURLp += '&dealer=' + this.userdetails._id;
      }
    }
    this.loadingCtrl.create({
      keyboardClose: true, message: 'Loging in...'
    }).then(loadingEl => {
      loadingEl.present();
      this.appService.getService(baseURLp)
        .subscribe(resp => {
          loadingEl.dismiss();
          that.portstemp = resp['devices'];
          that.mapData = [];
          that.mapData = that.portstemp.map(function (d) {
            if (d.last_location != undefined) {
              return { lat: d.last_location['lat'], lng: d.last_location['long'] };
            }
          });

          let bounds = new LatLngBounds(that.mapData);

          // that.allData.map.moveCamera({
          //   target: bounds,
          //   zoom: 10
          // });
          that.map.moveCamera({
            target: bounds
          })
          for (var i = 0; i < resp['devices'].length; i++) {
            // console.log("status: ", resp['devices'][i].status)
            if (resp['devices'][i].status != "Expired")
              that.socketInit(resp['devices'][i]);
          }
        });
    });
  }

  socketInit(pdata, center = false) {
    let that = this;
    that.socket.emit('acc', pdata.Device_ID);
    that.socketChnl.push(pdata.Device_ID + 'acc');
    // debugger
    // that.appService.getData(pdata.Device_ID + 'acc').subscribe((data) => {
    //   // debugger
    //   console.log('User data', data);
    // })
    that.socket.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
      if (d4 != undefined)
        (function (data) {

          if (data == undefined) {
            return;
          }

          if (data._id != undefined && data.last_location != undefined) {
            var key = data._id;
            that.impkey = data._id;
            that.deviceDeatils = data;
            let ic = _.cloneDeep(that.icons[data.iconType]);
            if (!ic) {
              return;
            }
            ic.path = null;
            if (data.status.toLowerCase() === 'running' && data.last_speed > 0) {
              if (that.plt.is('ios')) {
                ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
              } else if (that.plt.is('android')) {
                ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
              }
            } else {
              if (data.status.toLowerCase() === 'running' && data.last_speed === 0) {
                if (that.plt.is('ios')) {
                  ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                } else if (that.plt.is('android')) {
                  ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
              } else {
                if (that.plt.is('ios')) {
                  ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                } else if (that.plt.is('android')) {
                  ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                }
              }
            }

            console.log("ic url=> " + ic.url)
            that.vehicle_speed = data.last_speed;
            that.todays_odo = data.today_odo;
            that.total_odo = data.total_odo;
            // that.fuel = data.currentFuel;
            if (that.userdetails.fuel_unit == 'LITRE') {
              that.fuel = data.currentFuel;
            } else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
              that.fuel = data.fuel_percent;
            } else {
              that.fuel = data.currentFuel;
            }
            that.last_ping_on = data.last_ping_on;

            if (data.lastStoppedAt != null) {
              var fd = new Date(data.lastStoppedAt).getTime();
              var td = new Date().getTime();
              var time_difference = td - fd;
              var total_min = time_difference / 60000;
              var hours = total_min / 60
              var rhours = Math.floor(hours);
              var minutes = (hours - rhours) * 60;
              var rminutes = Math.round(minutes);
              that.lastStoppedAt = rhours + ':' + rminutes;
            } else {
              that.lastStoppedAt = '00' + ':' + '00';
            }

            that.distFromLastStop = data.distFromLastStop;
            if (!isNaN(data.timeAtLastStop)) {
              that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
            } else {
              that.timeAtLastStop = '00:00:00';
            }
            that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
            that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
            that.last_ACC = data.last_ACC;
            that.acModel = data.ac;
            that.currentFuel = data.currentFuel;
            that.power = data.power;
            that.gpsTracking = data.gpsTracking;
            that.recenterMeLat = data.last_location.lat;
            that.recenterMeLng = data.last_location.long;

            if (that.allData[key]) {
              that.socketSwitch[key] = data;
              if (that.allData[key].mark != undefined) {
                that.allData[key].mark.setIcon(ic);
                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                var temp = _.cloneDeep(that.allData[key].poly[1]);
                that.allData[key].poly[0] = _.cloneDeep(temp);
                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                that.liveTrack(that.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
              } else {
                return;
              }
            }
            else {
              that.allData[key] = {};
              that.socketSwitch[key] = data;
              that.allData[key].poly = [];

              that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });

              if (data.last_location != undefined) {

                that.map.addMarker({
                  title: data.Device_Name,
                  position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                  icon: ic,
                }).then((marker: Marker) => {

                  that.allData[key].mark = marker;
                  marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                    .subscribe(e => {
                      if (that.selectedVehicle == undefined) {
                      } else {
                        that.liveVehicleName = data.Device_Name;
                        that.showaddpoibtn = true;
                        that.drawerState = DrawerState.Docked;
                        that.onClickShow = true;
                      }
                    });
                  var speed = data.status == "RUNNING" ? data.last_speed : 0;
                  that.liveTrack(that.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                })
              }
            }
          }
        })(d4)
    })
  }

  liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {

    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);

    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        mark.setIcon(icons);
      }
      function _moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {
          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(new LatLng(lat, lng));
          }
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.selectedVehicle != undefined) {
            map.setCameraTarget(dest);
          }
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }


  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present();
  }
  // ngOnInit() {
  //   this.socket.connect();
  //   this.ionicStorage.get('token').then((val) => {
  //     console.log('Your age is', val);
  //     this.userdetails = val;
  //     this.showBtn = false;
  //     this.SelectVehicle = "Select Vehicle";
  //     this.selectedVehicle = undefined;
  //     this.userDevices();
  //   })
  //   this.drawerState = DrawerState.Bottom;
  //   this.onClickShow = false;
  // }

  ngOnDestroy() {
    this.socket.disconnect();
    // for (var i = 0; i < this.socketChnl.length; i++)
    //   this.socket.removeAllListeners(this.socketChnl[i]);
    // this.socket.on('disconnect', () => {
    //   // this.socket.open();
    // })
  }

  zoomin() {
    let that = this;
    that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    return h + ':' + m + ':' + s;
  }

  onDismiss() {
    this.modalCtrl.dismiss();
  }

  reCenterMe() {
    // console.log("getzoom level: " + this.allData.map.getCameraZoom());
    this.allData.map.moveCamera({
      target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
      zoom: this.allData.map.getCameraZoom()
    }).then(() => {

    })
  }

  temp(data) {
    // debugger
    if (data.status == 'Expired') {
 
    } else {
      let that = this;
      that.liveDataShare = data;
      // that.drawerHidden = true;
      that.drawerState = DrawerState.Bottom;
      that.onClickShow = false;

      if (that.allData.map != undefined) {
        // that.allData.map.clear();
        that.allData.map.remove();
      }

      console.log("on select change data=> " + JSON.stringify(data));
      for (var i = 0; i < that.socketChnl.length; i++)
        that.socket.removeAllListeners(that.socketChnl[i]);
      that.allData = {};
      that.socketChnl = [];
      that.socketSwitch = {};
      if (data) {
        if (data.last_location) {

          let mapOptions = {
            // backgroundColor: 'white',
            controls: {
              compass: true,
              zoom: false,
              mapToolbar: true
            },
            gestures: {
              rotate: false,
              tilt: false
            },
            mapType: that.mapKey
          }
          let map = GoogleMaps.create('map_canvas', mapOptions);
          map.animateCamera({
            target: { lat: 20.5937, lng: 78.9629 },
            zoom: 15,
            duration: 1000,
            padding: 0  // default = 20px
          });

          map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
          map.setPadding(20, 20, 20, 20);
          // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
          that.allData.map = map;
          that.socketInit(data);
        } else {
          that.allData.map = that.newMap();
          that.socketInit(data);
        }

        if (that.selectedVehicle != undefined) {
          // that.drawerHidden = false;
          that.drawerState = DrawerState.Docked;
          that.onClickShow = true;
        }
      }
      that.showBtn = true;
    }
  }
  info(mark, cb) {
    mark.addListener('click', cb);
  }

  setDocHeight() {
    let that = this;
    that.drawerState = DrawerState.Top;
  }

  onMapChange() {

  }
}

