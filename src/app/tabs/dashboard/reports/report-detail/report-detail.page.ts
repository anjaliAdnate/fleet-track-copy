import { Component, OnInit } from "@angular/core";
import { AppService } from "src/app/app.service";
import { URLs } from "src/app/app.model";
import { Storage } from "@ionic/storage";
import { LoadingController } from "@ionic/angular";
import * as moment from "moment";
import { ActivatedRoute } from "@angular/router";
import { GeocoderAPIService } from 'src/app/geocoder-api.service';

@Component({
  selector: "app-report-detail",
  templateUrl: "./report-detail.page.html",
  styleUrls: ["./report-detail.page.scss"]
})
export class ReportDetailPage implements OnInit {
  report_name: any;
  overSpeeddevice_id: any;
  ignitiondevice_id: any;
  device_id: any;
  overspeedReport: any[] = [];
  stoppageReport: any[] = [];
  summaryReport: any[] = [];
  tripReport: any[] = [];
  userdetails: any;
  portstemp: any;
  datetimeStart: string;
  datetimeEnd: string;
  url: string;
  constructor(
    private appService: AppService,
    private constURL: URLs,
    private ionicStorage: Storage,
    private loadingCtrl: LoadingController,
    private activateRoute: ActivatedRoute,
    private geocoderApi: GeocoderAPIService
  ) {
    this.datetimeStart = moment({ hours: 0 }).format();
    console.log("start date", this.datetimeStart);
    this.datetimeEnd = moment().format();
    console.log("stop date", this.datetimeEnd);

    this.activateRoute.paramMap.subscribe(parammap => {
      if (!parammap.get("id")) {
        return;
      }
      this.report_name = parammap.get("id");

      console.log("report name", this.report_name);
    });

    this.url = this.constURL.mainUrl + 'gps/getaddress';

  }

  ngOnInit() {
    this.ionicStorage.get("token").then(val => {
      console.log("Your age is", val);
      this.userdetails = val;
      if (this.report_name === "Overspeed") {
        this.getReport(this.report_name);
      } else if (this.report_name === "Summary") {
        this.getReport(this.report_name);
      } else if (this.report_name === "Stoppage") {
        this.getReport(this.report_name);
      } else if (this.report_name === "Trip") {
        this.getReport(this.report_name);
      }
    });
  }

  getReport(reportId) {
    console.log("inside", reportId);
    if (reportId === 'Overspeed') {
      this.getOverSpeedReport();
    }
    if (reportId === 'Stoppage') {
      this.getStoppageReport();
    }
    if (reportId === 'Summary') {
      this.getSummaryReport();
    }
    if (reportId === 'Trip') {
      this.getTripReport();
    }
  }


  getOverspeeddevice(selectedVehicle) {
    console.log(selectedVehicle);
    this.overSpeeddevice_id = selectedVehicle.Device_ID;
  }
  getIgnitiondevice(selectedVehicle) {
    this.ignitiondevice_id = selectedVehicle.Device_ID;
  }
  getdevice_id(selectedVehicle) {
    this.device_id = selectedVehicle.Device_ID;
  }

  getdevices() {
    var baseURLp =
      this.constURL.mainUrl + "devices/getDeviceByUser?id=" +
      this.userdetails._id +
      "&email=" +
      this.userdetails.email;

    if (this.userdetails.isSuperAdmin == true) {
      baseURLp += "&supAdmin=" + this.userdetails._id;
    } else {
      if (this.userdetails.isDealer == true) {
        baseURLp += "&dealer=" + this.userdetails._id;
      }
    }
    let that = this;
    this.loadingCtrl
      .create({
        keyboardClose: true,
        message: "Loading Vehicle List.."
      })
      .then(loadingEl => {
        loadingEl.present();
        this.appService.getService(baseURLp).subscribe(resp => {
          loadingEl.dismiss();
          that.portstemp = resp["devices"];
        });
      });
  }

  getOverSpeedReport() {
    var baseUrl;
    if (this.overSpeeddevice_id === undefined) {
      baseUrl =
        this.constURL.mainUrl + "notifs/overSpeedReport?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&_u=" +
        this.userdetails._id;
    } else {
      baseUrl =
        this.constURL.mainUrl + "notifs/overSpeedReport?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&_u=" +
        this.userdetails._id +
        "&device=" +
        this.overSpeeddevice_id;
    }
    const that = this;
    this.loadingCtrl
      .create({
        keyboardClose: true,
        message: "Loading Overspeed Report"
      })
      .then(loadingEl => {
        loadingEl.present();
        this.appService.getService(baseUrl).subscribe(resp => {
          console.log(resp);
          loadingEl.dismiss();
          if (JSON.parse(JSON.stringify(resp)).length > 0) {
            this.innerFunc(JSON.parse(JSON.stringify(resp)));
          }
        });
      });
  }

  innerFunc(data) {
    let outerthis = this;
    var i = 0, howManyTimes = data.length;
    function f() {

      outerthis.overspeedReport.push({
        'vehicleName': data[i].vehicleName,
        'overSpeed': data[i].overSpeed,
        'start_location': {
          'lat': data[i].lat,
          'long': data[i].long
        },
      });
      outerthis.start_address(outerthis.overspeedReport[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  start_address(item, index) {
    let that = this;
    that.overspeedReport[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.overspeedReport[index].StartLocation = "N/A";
      return;
    }
    let tempcord = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.appService.getAddress(tempcord)
      .subscribe(resp => {
        var res = JSON.parse(JSON.stringify(resp));
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.overspeedReport[index].StartLocation = str;
            })
        } else {
          that.overspeedReport[index].StartLocation = res.address;
        }
      })
  }

  getStoppageReport() {
    var base;
    let th = this;
    if (this.ignitiondevice_id === undefined) {
      base =
        this.constURL.mainUrl + "stoppage/stoppageReport?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&_u=" +
        this.userdetails._id;
    } else {
      base =
        this.constURL.mainUrl + "stoppage/stoppageReport?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&_u=" +
        this.userdetails._id +
        "&device=" +
        this.ignitiondevice_id;
    }

    this.loadingCtrl
      .create({
        keyboardClose: true,
        message: "Fetching data..."
      })
      .then(loadingEl => {
        loadingEl.present();
        this.appService.getService(base).subscribe(resp => {
          console.log(resp);
          loadingEl.dismiss();
          if (JSON.parse(JSON.stringify(resp)).length > 0) {
            this.innerFunc1(JSON.parse(JSON.stringify(resp)));
          }
        });
      });
  }

  innerFunc1(data) {
    let outerthis = this;
    var i = 0, howManyTimes = data.length;
    function f() {
      var fd = new Date(data[i].arrival_time).getTime();
      var td = new Date(data[i].departure_time).getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      var Durations = rhours + 'hrs : ' + rminutes + 'mins';

      outerthis.stoppageReport.push({
        'arrival_time': data[i].arrival_time,
        'departure_time': data[i].departure_time,
        'Durations': Durations,
        'device': data[i].device ? data[i].device.Device_Name : 'N/A',
        'end_location': {
          'lat': data[i].lat,
          'long': data[i].long
        }
      });
      outerthis.end_address(data[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  end_address(item, index) {
    let that = this;
    that.stoppageReport[index].EndLocation = "N/A";
    if (!item.end_location) {
      that.stoppageReport[index].EndLocation = "N/A";
      return;
    } 
    let tempcord1234 = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.appService.getAddress(tempcord1234)
      .subscribe(resp => {
        var res = JSON.parse(JSON.stringify(resp));
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.stoppageReport[index].EndLocation = str;
            });
        } else {
          that.stoppageReport[index].EndLocation = res.address;
        }
      });
  }

  getSummaryReport() {
    var basep;
    let th = this;
    if (this.device_id === undefined) {
      basep =
        this.constURL.mainUrl + "summary/summaryReport?from=" +
        new Date(this.datetimeStart).toISOString() +
        "&to=" +
        new Date(this.datetimeEnd).toISOString() +
        "&user=" +
        this.userdetails._id;
    } else {
      basep =
        this.constURL.mainUrl + "summary/summaryReport?from=" +
        new Date(this.datetimeStart).toISOString() +
        "&to=" +
        new Date(this.datetimeEnd).toISOString() +
        "&user=" +
        this.userdetails._id +
        "&device=" +
        this.device_id;
    }
    this.loadingCtrl
      .create({
        keyboardClose: true,
        message: "Fetching data..."
      })
      .then(loadingEl => {
        loadingEl.present();
        this.appService.getService(basep).subscribe(resp => {
          loadingEl.dismiss();
          if (JSON.parse(JSON.stringify(resp)).length > 0) {
            this.innerFunc2(JSON.parse(JSON.stringify(resp)));
          }
        });
      });
  }
  innerFunc2(data) {
    let outerthis = this;
    var i = 0, howManyTimes = data.length;
    function f() {
      // console.log("conversion: ", Number(outerthis.summaryReport[i].devObj[0].Mileage))
      var hourconversion = 2.7777778 / 10000000;
      outerthis.summaryReport.push(
        {
          'Device_Name': data[i].devObj[0].Device_Name,
          'routeViolations': data[i].today_routeViolations,
          'overspeeds': (data[i].today_overspeeds * hourconversion).toFixed(2),
          'ignOn': (data[i].today_running * hourconversion).toFixed(2),
          'ignOff': (data[i].today_stopped * hourconversion).toFixed(2),
          'distance': data[i].today_odo,
          'tripCount': data[i].today_trips,
          'mileage': data[i].devObj[0].Mileage ? ((data[i].today_odo) / Number(data[i].devObj[0].Mileage)).toFixed(2) : 'N/A',
          'end_location': data[i].end_location,
          'start_location': data[i].start_location
        });

      outerthis.s_address(outerthis.summaryReport[i], i);
      outerthis.e_address(outerthis.summaryReport[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  s_address(item, index) {
    let that = this;
    that.summaryReport[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.summaryReport[index].StartLocation = "N/A";
    } 
    let tempcord1234 = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.appService.getAddress(tempcord1234)
      .subscribe(resp => {
        var res = JSON.parse(JSON.stringify(resp));
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.summaryReport[index].StartLocation = str;
            });
        } else {
          that.summaryReport[index].StartLocation = res.address;
        }
      });
    // else if (item.start_location) {
    //   // debugger
    //   var payload = {
    //     "lat": item.start_location.lat,
    //     "long": item.start_location.long,
    //     "api_id": "2"
    //   }
    //   this.appService.getServiceWithParams(this.url, payload)
    //     .subscribe((data) => {
    //       // console.log("got address: " + data.results)
    //       var d = JSON.parse(JSON.stringify(data));
    //       if (d.results[2] != undefined || d.results[2] != null) {
    //         that.summaryReport[index].StartLocation = d.results[2].formatted_address;
    //       } else {
    //         that.summaryReport[index].StartLocation = 'N/A';
    //       }

    //     })
    // }
  }

  e_address(item, index) {
    let that = this;
    that.summaryReport[index].EndLocation = "N/A";
    if (!item.end_location) {
      that.summaryReport[index].EndLocation = "N/A";
      return;
    }
    let tempcord123 = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.appService.getAddress(tempcord123)
      .subscribe(resp => {
        var res = JSON.parse(JSON.stringify(resp));
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.summaryReport[index].EndLocation = str;
            });
        } else {
          that.summaryReport[index].EndLocation = res.address;
        }
      });
    // else if (item.end_location) {
    //   var payload = {
    //     "lat": item.end_location.lat,
    //     "long": item.end_location.long,
    //     "api_id": "2"
    //   }
    //   this.appService.getServiceWithParams(this.url, payload)
    //     .subscribe((data) => {
    //       var d = JSON.parse(JSON.stringify(data));
    //       // console.log("got address: " + data.results)
    //       if (d.results[2] != undefined || d.results[2] != null) {
    //         that.summaryReport[index].EndLocation = d.results[2].formatted_address;
    //       } else {
    //         that.summaryReport[index].EndLocation = 'N/A';
    //       }

    //     })
    // }
  }

  getTripReport() {
    var baset;
    let th = this;
    if (this.device_id === undefined) {
      baset =
        this.constURL.mainUrl + "user_trip/trip_detail?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&uId=" +
        this.userdetails._id;
    } else {
      baset =
        this.constURL.mainUrl + "user_trip/trip_detail?from_date=" +
        new Date(this.datetimeStart).toISOString() +
        "&to_date=" +
        new Date(this.datetimeEnd).toISOString() +
        "&uId=" +
        this.userdetails._id +
        "&device=" +
        this.device_id;
    }
    this.loadingCtrl
      .create({
        keyboardClose: true,
        message: "Fetching data..."
      })
      .then(loadingEl => {
        loadingEl.present();
        this.appService.getService(baset).subscribe(resp => {
          console.log(resp);
          loadingEl.dismiss();
          if (JSON.parse(JSON.stringify(resp)).length > 0) {
            this.tripFunction(JSON.parse(JSON.stringify(resp)));
          }
          // th.tripReport = JSON.parse(JSON.stringify(resp));
        });
      });
  }
  tripFunction(data) {
    let that = this;
    var i = 0, howManyTimes = data.length;
    function f() {
      var deviceId = data[i]._id;
      var distanceBt = data[i].distance / 1000;

      var gmtDateTime = moment.utc(JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
      var gmtDate = moment.utc(JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
      var Startetime = gmtDateTime.local().format(' h:mm a');
      var Startdate = gmtDate.format('ll');
      var gmtDateTime1 = moment.utc(JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
      var gmtDate1 = moment.utc(JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
      var Endtime = gmtDateTime1.local().format(' h:mm a');
      var Enddate = gmtDate1.format('ll');

      var startDate = new Date(data[i].start_time).toLocaleString();
      var endDate = new Date(data[i].end_time).toLocaleString();

      var fd = new Date(startDate).getTime();
      var td = new Date(endDate).getTime();
      var time_difference = td - fd;
      var total_min = time_difference / 60000;
      var hours = total_min / 60
      var rhours = Math.floor(hours);
      var minutes = (hours - rhours) * 60;
      var rminutes = Math.round(minutes);
      var Durations = rhours + 'hrs' + ' ' + rminutes + 'mins';
      that.tripReport.push(
        {
          'Device_Name': data[i].device.Device_Name,
          'Device_ID': data[i].device.Device_ID,
          'Startetime': Startetime,
          'Startdate': Startdate,
          'Endtime': Endtime,
          'Enddate': Enddate,
          'distance': distanceBt,
          '_id': deviceId,
          'start_time': data[i].start_time,
          'end_time': data[i].end_time,
          'duration': Durations,
          'end_location': {
            'lat': data[i].end_lat,
            'long': data[i].end_long
          },
          'start_location': {
            'lat': data[i].start_lat,
            'long': data[i].start_long
          }
        });

      that.start_add(that.tripReport[i], i);
      that.end_add(that.tripReport[i], i);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    } f();
  }

  start_add(item, index) {
    let that = this;
    that.tripReport[index].StartLocation = "N/A";
    if (!item.start_location) {
      that.tripReport[index].StartLocation = "N/A";
      return;
    }
    let tempcord12 = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.appService.getAddress(tempcord12)
      .subscribe(resp => {
        var res = JSON.parse(JSON.stringify(resp));
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.tripReport[index].StartLocation = str;
            });
        } else {
          that.tripReport[index].StartLocation = res.address;
        }
      });
    // else if (item.start_location) {
    //   // debugger
    //   var payload = {
    //     "lat": item.start_location.lat,
    //     "long": item.start_location.long,
    //     "api_id": "2"
    //   }
    //   this.appService.getServiceWithParams(this.url, payload)
    //     .subscribe((data) => {
    //       var d = JSON.parse(JSON.stringify(data));
    //       // console.log("got address: " + data.results)
    //       if (d.results[2] != undefined || d.results[2] != null) {
    //         that.tripReport[index].StartLocation = d.results[2].formatted_address;
    //       } else {
    //         that.tripReport[index].StartLocation = 'N/A';
    //       }

    //     })
    // }
  }

  end_add(item, index) {
    let that = this;
    that.tripReport[index].EndLocation = "N/A";
    if (!item.end_location) {
      that.tripReport[index].EndLocation = "N/A";
      return;
    }
    let tempcord1 = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.appService.getAddress(tempcord1)
      .subscribe(resp => {
        var res = JSON.parse(JSON.stringify(resp));
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.tripReport[index].EndLocation = str;
            });
        } else {
          that.tripReport[index].EndLocation = res.address;
        }
      });
  }
}
