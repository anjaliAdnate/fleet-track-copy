import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PushObject, PushOptions, Push } from '@ionic-native/push/ngx';
import { ToastController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AppService } from '../app.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  token: string;
  userToken: any = {};

  constructor(
    public router: Router,
    private toastCtrl: ToastController,
    private push: Push,
    private platform: Platform,
    private ionicStorage: Storage,
    private apiCall: AppService) { }

  ngOnInit() {
  }

  pushSetup() {
    this.push.createChannel({
      id: "ignitionon",
      description: "ignition on description",
      sound: 'ignitionon',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('Channel created'));
    this.push.createChannel({
      id: "ignitionoff",
      description: "ignition off description",
      sound: 'ignitionoff',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('ignitionoff Channel created'));

    this.push.createChannel({
      id: "devicepowerdisconnected",
      description: "devicepowerdisconnected description",
      sound: 'devicepowerdisconnected',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('devicepowerdisconnected Channel created'));

    this.push.createChannel({
      id: "default",
      description: "default description",
      sound: 'default',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('default Channel created'));

    this.push.createChannel({
      id: "notif",
      description: "default description",
      sound: 'notif',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 4
    }).then(() => console.log('sos Channel created'));
    // Delete a channel (Android O and above)
    // this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));

    // Return a list of currently configured channels
    this.push.listChannels().then((channels) => console.log('List of channels', channels))
    let that = this;
    // const options: PushOptions = {
      const options: PushOptions = {
        android: {},
        ios: {
          alert: 'true',
          badge: true,
          sound: 'false'
        },
        windows: {},
        browser: {
          pushServiceURL: 'http://push.api.phonegap.com/v1/push'
        }
      }

    const pushObject: PushObject = that.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      if (notification.additionalData.foreground) {
        this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        }).then((toastEl)=> {
          toastEl.present();
        })
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        console.log("dashboard entered device token => " + registration.registrationId);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        that.addPushNotify();
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
    });
  }
  ionViewWillEnter() {
    this.ionicStorage.get('token').then((val) => {
      console.log('Your age is', val);
      this.userToken = val;
      this.addPushNotify();
    })
  }

  addPushNotify() {
    // console.log("VeryFirstLoginUser: ", localStorage.getItem("VeryFirstLoginUser"));
    let that = this;
    that.token = "";
    var pushdata;
    // debugger;
    that.token = localStorage.getItem("DEVICE_TOKEN");
    var v_id = localStorage.getItem("VeryFirstLoginUser");
    if (that.token == null) {
      this.pushSetup();
    } else {
      if (this.platform.is('android')) {
        pushdata = {
          "uid": that.userToken._id,
          "token": that.token,
          "os": "android"
        }
      } else {
        pushdata = {
          "uid": that.userToken._id,
          "token": that.token,
          "os": "ios"
        }
      }

      if (v_id == that.userToken._id) {
        var url = "https://www.oneqlik.in/users/PushNotification";
        that.apiCall.getServiceWithParams(url, pushdata)
          .subscribe(data => {
            console.log("push notifications updated " + JSON.parse(JSON.stringify(data)).message)
          },
            error => {
              console.log(error);
            });
      }
      else {
        console.log("Token already updated!!")
      }
    }
  }
}
