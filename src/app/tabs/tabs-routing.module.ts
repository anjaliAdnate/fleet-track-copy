import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page";
// import { SecondTabsPage } from './dashboard/vehicle-list/second-tabs/second-tabs.page';

const routes: Routes = [
  {
    path: "tabs",
    component: TabsPage,
    children: [
      {
        path: "dashboard",
        children: [
          {
            path: "",
            loadChildren: "./dashboard/dashboard.module#DashboardPageModule"
          },
          {
            path: "live",
            loadChildren: "./dashboard/live/live.module#LiveComponentModule"
          }
        ]
      },
      {
        path: 'vehicle-list',
        children: [
          {
            path: '',
            loadChildren: "./dashboard/vehicle-list/vehicle-list.module#VehicleListPageModule"
          },
          {
            path: ':id',
            loadChildren: "./dashboard/vehicle-list/vehicle-list.module#VehicleListPageModule"
          },
          {
            path: "second-tabs",
            loadChildren:
              "./dashboard/vehicle-list/second-tabs/second-tabs.module#SecondTabsPageModule"
          }
        ]
      },
      {
        path: "reports",
        children: [
          {
            path: "",
            loadChildren: "./dashboard/reports/reports.module#ReportsPageModule"
          },
          {
            path: ":id",
            loadChildren:
              "./dashboard/reports/report-detail/report-detail.module#ReportDetailPageModule"
          },
          {
            path: "",
            redirectTo: "/maintabs/tabs/dashboard",
            pathMatch: "full"
          }
        ]
      },
      {
        path: "alerts",
        loadChildren: "./dashboard/alerts/alerts.module#AlertsPageModule"
      },
      {
        path: "settings",
        children: [
          {
            path: '',
            loadChildren: './dashboard/settings/settings.module#SettingsPageModule'
          },
          {
            path: 'configure',
            loadChildren: './dashboard/settings/configure/configure.module#ConfigurePageModule'
          },
          {
            path: 'change-password',
            loadChildren: './dashboard/settings/change-password/change-password.module#ChangePasswordPageModule'
          },
          {
            path: 'support',
            loadChildren: './dashboard/settings/support/support.module#SupportPageModule'
          }
        ]
      },
      {
        path: "",
        redirectTo: "/maintabs/tabs/dashboard",
        pathMatch: "full"
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsRoutingModule { }
